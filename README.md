Quick little script to check the earliest available date for an appointment at ICBC.

usage

```
npm install
node checker.js
```



sample output

![](https://i.imgur.com/oYSztmK.png)