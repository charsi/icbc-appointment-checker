"use strict";

const fetch = require('node-fetch');

const baseUrl = 'https://onlinebusiness.icbc.com/qmaticwebbooking/rest/schedule'

const serviceID = {
  singleKnowledgeTest: 'da8488da9b5df26d32ca58c6d6a7973bedd5d98ad052d62b468d3b04b080ea25',
  comboKnowledgeTest: '895063ac05ab8f9a4791fd0e8198d3e198c78de532dc7c985f4360a3ed080599',
  renewLicence: 'c2a883f450951e191132a1dc0ccc3606293b454c65b72b81ac304eefae19e709',
  somethingElse: '9d140201d792c24c15884ac50bbb52b6297768c05aad2edf40d81ca47a6e502d'
}

// These Branches will be skipped
const branchesTooFar = [
  'Kamloops',
  'Nanaimo',
  'Victoria - McKenzie Ave',
  'Victoria - Wharf St',
  'Kelowna',
  'Guildford'
];

main();

async function main() {
  let branches = await getBranches(baseUrl);
  let matchingBranches = [];
  for (let branch of branches) {
    if (!branchesTooFar.includes(branch.branchName)) {
      console.log(`Checking ${branch.branchName}..`);
      branch.earliestDate = await getEarliestDate(baseUrl, branch.branchPublicId, serviceID.singleKnowledgeTest);
      matchingBranches.push(branch);
    }
  }

  matchingBranches.sort((a, b) => {
    return new Date(a.earliestDate) - new Date(b.earliestDate);
  });

  console.log('\nEarliest Dates\n' +
    '==============');
  matchingBranches.forEach((branch) => {
    if (branch.earliestDate) {
      let days = Math.floor((new Date(branch.earliestDate)-Date.now())/1000/60/60/24);
      console.log(`${branch.earliestDate} in ${days} days at ${branch.branchName} `);
    }
  });
}


async function getBranches(url) {
  const branchesUrl = url + '/appointmentProfiles';
  let response = await fetch(branchesUrl);
  if (response.ok) {
    return response.json();
  } else {
    return 'Error loading ICBC page';
  }
}

async function getEarliestDate(url, branchID, serviceID) {
  const datesUrl = `${url}/branches/${branchID}/dates;servicePublicId=${serviceID};customSlotLength=15`;
  let response = await fetch(datesUrl)
  if (response.ok) {
    return (await response.json())[0].date;
  } else {
    return 'Error loading ICBC page';
  }
}


